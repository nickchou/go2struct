# go2struct

目前主要是生成gorm的struct实体，支持的驱动目前仅限于mysql或者mariadb


## 使用说明一、源码安装
1. 本地安装好golang，并检查window环境变量，确保配置和GOPATH和PATH环境变量，如图
![输入图片说明](https://images.gitee.com/uploads/images/2019/1127/154949_3f5db1e8_90222.jpeg "GOPATH环境变量.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1127/155009_a362d4c6_90222.jpeg "PATH环境变量.jpg")
2. go get安装
```
go get -u gitee.com/nickchou/go2struct
```
安装完毕后%GOPATH%/bin目录下可以看到go2struct.exe

3. 进入指定的文件夹下执行指令（如F:/test）
```
go2struct mysql "root:123456@(127.0.0.1:3306)/scriptdb?charset=utf8" %GOPATH%/src/gitee.com/nickchou/go2struct/tpl/gorm.tpl ./models
```
也可以吧%GOPATH%/src/gitee.com/nickchou/go2struct/tpl下的gorm.tpl文件复制到指定文件指定命令（如F:/test）
```
go2struct mysql "root:123456@(127.0.0.1:3306)/scriptdb?charset=utf8" ./gorm.tpl ./models
```
生成后的截图
![输入图片说明](https://images.gitee.com/uploads/images/2019/1127/160942_1bf33c3e_90222.jpeg "4444.jpg")
4. 参数说明，看图其实基本就明白了
> 参数1：驱动，目前只支持mysql

> 参数2：数据库链接，自行修改

> 参数3：tpl模板文件的位置

> 参数4：生成struct存放的位置

![输入图片说明](https://images.gitee.com/uploads/images/2019/1127/160141_0f924f0d_90222.jpeg "333.jpg")
5.  gorm.tpl是可以修改的，用的是golang标准的html/template语法，想生成什么可以自行修改