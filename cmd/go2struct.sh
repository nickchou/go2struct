#!/bin/sh
##################################################
#  注意事项：
# 【shell脚本执行错误 $'\r':command not found】
# 1、yum install -y dos2unix
# 2、dos2unix dk-goadmin.sh
#
# sh dk-goadmin.sh
##################################################
#  变量部分
##################################################
GO_PATH="${GOPATH}"       # 获取系统环境变量
##################################################
#  函数部分
##################################################
#打印白色字体
function show (){
    echo -e "$1"  #默认颜色
}
#打印绿色字体
function show_green (){
    echo -e "\033[32m$1\033[0m"     #绿色
}
#打印红色字体
function show_red (){
    echo -e "\033[31m$1\033[0m"     #红色
}
#打印当前文件夹
function show_dir (){
    curr_path=`pwd`
    show "Curr Path : $curr_path"
}
#暂停系统，并用红色字打印
function pause_system (){
    show_red "$1"
    read -p ""  V_P
    exit 0
}
function close_system (){
    show_green "$1"
    read -p ""  V_P
    exit 0
}
function check_cmd() {
    if [ $? -ne 0 ];then
        pause_system "$1 error!"
    else
        show_green "$1 ok"
    fi
}
#==========================================================
#  Command   
#==========================================================
show_green "go2struct convert db to gorm struct:"
#1、检查并安装必要的go2struct工具
if [ ! -f "$GO_PATH/bin/go2struct.exe" ]; then
  show_green "installing go2struct..."
  go get -u gitee.com/nickchou/go2struct
  check_cmd "install go2struct:"
fi
#2、根据数据库连接生成代码
go2struct mysql "root:123456@(127.0.0.1:3306)/sys?charset=utf8" ./gorm.tpl ./models
#==========================================================
#  Command   
#==========================================================
close_system "press any key to exit"