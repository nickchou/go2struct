module gitee.com/nickchou/go2struct

go 1.16

require (
	gorm.io/driver/mysql v1.1.0
	gorm.io/gorm v1.21.9
)
